import json
import re
import unittest

import pytz

from handler import get_date_in_multiple_timezone


class TestHandler(unittest.TestCase):
    def test_timezone_has_all_timezones(self):
        result = get_date_in_multiple_timezone(event=None, context=None)
        json_object = json.loads(result)
        zone_names = pytz.all_timezones
        assert list(json_object.keys()) == zone_names

    def test_timezone_has_correct_date_time_format(self):
        result = get_date_in_multiple_timezone(event=None, context=None)
        json_object = json.loads(result)
        zone_names = list(json_object.keys())
        regex = r'^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\.[0-9]+)?(Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])?$'
        match_iso8601 = re.compile(regex).match
        for zone in zone_names:
            date_time = json_object[zone]
            assert match_iso8601(date_time) is not None
